Problem Statement: 
Given the schedule of Task A, find the minimum number of times that a Task B (which runs for n hours) must be scheduled such that a Task B is completed in the last m hours or less before every occurrence of Task A. 

Assumptions:
1) The schedule for Task A is a series of natural numbers, not constrained by the 24-hour day format. So the numbers can be 1, 2, ... 24, 25,.. and so on.
2) Granularity of time is 1 hour
3) Since the constraint is that a task B must be COMPLETED in m hours or less for the next task A, it is possible that 2 or more task Bs can be running at the same time if n > m
4) In the constraint - "A task B must be completed in the last m hours or less before next task A", m is inclusive.


Input Format:
m
n
size of array containing Task A schedule
whitespace separated values for Task A schedule

Sample Input:
2
5
6
10 11 12 13 16 19

Expected Output:
Number of runs of B required: 
4
Start time schedule for B:
5
8
11
14


Ideone link for convenience: http://ideone.com/fork/aanh3C



