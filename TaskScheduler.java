import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskScheduler {
	
	public static List<Integer> aSchedule;		
	public static int m;	//Task B must be have completed m hours or less before task A
	public static int n;	//Duration for which task B runs
	public static List<Integer> bStartSchedule;
	
	public static void scheduleTaskB() {
	    bStartSchedule = new ArrayList<Integer>();
	    int bCount = 0;		//No. of times B has run so far
	    
	    if(!aSchedule.isEmpty()) {
	    	
			/* bStartSchedule refers to the max time when B MUST start.
		    	For e.g., if first occurrence of task A is at 10 am and B takes 3 hours to be completed, 
		    	i.e. n = 3, then B MUST start by 10 - 3 = 7 am 
		    */
	    	bStartSchedule.add(aSchedule.get(0) - n);
	    	bCount ++;
	    	
		    for (Integer timeA: aSchedule) {
		    	//(start time of A - end time of B) should be less than or equal to m
		        if((timeA - (bStartSchedule.get(bCount - 1) + n )) > m) {
			    	bStartSchedule.add(timeA - n);
			    	bCount ++;
		        }
		    }
		    System.out.println("Number of runs of B required: \n" + bCount);
		    System.out.println("Start time schedule for B:");
		    for(Integer timeB: bStartSchedule) {
		    	System.out.println(timeB);
		    }
		    
	    } else {
	    	System.out.println("No runs of A are scheduled, so no runs of B required!");
	    }
	}
	
	public static void fillSchedule() {
		aSchedule = new ArrayList<Integer>();
		Scanner sc = new Scanner(System.in);
		m = sc.nextInt();
		n = sc.nextInt();
		int aCount = sc.nextInt();	
		for(int i = 0; i < aCount; i ++) {
			aSchedule.add(sc.nextInt());
		}
		sc.close();
		
	}

	public static void main(String args[]) {
		fillSchedule();
		scheduleTaskB();
	}


}
